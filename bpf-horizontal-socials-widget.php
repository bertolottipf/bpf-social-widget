<?php
/**
 * Plugin Name: BPF Horizontal Socials
 * Description: A widget that show socials icons in horizontal.
 * Version: 0.1
 * Author: BERTOLOTTI Paolo Francesco
 * Author URI: 
 */

// aggiunge il widget all'elenco di quelli già presenti
add_action( 'widgets_init', 'bpf_horizontal_socials_widget' );

function bpf_horizontal_socials_widget() {
	register_widget( 'BPF_Horizontal_Socials_Widget' );
}

class BPF_Horizontal_Socials_Widget extends WP_Widget {
	
	
	// costruttore
	function BPF_Horizontal_Socials_Widget() {
		$widget_ops = array('description' => 'A widget that show socials icons in horizontal');
		$this->WP_Widget('bpf-horizontal-socials-widget', 'BPF HORIZONTAL SOCIALS WIDGET', $widget_ops);
	}
	
	
	// genera il form delle impostazioni del widget nel back-end
	function form( $instance ) {
		$facebook = esc_attr( $instance['facebook'] );
		$twitter = esc_attr( $instance['twitter'] );
		$linkedin = esc_attr( $instance['linkedin'] );
		$googleplus = esc_attr( $instance['googleplus'] );
		$youtube =  esc_attr( $instance['youtube'] );
		$email =  esc_attr( $instance['email'] );
		$feed = esc_attr( $instance['feed'] );
		?>
		<p>
			<label for="<?php echo $this -> get_field_id('facebook'); ?>">Facebook link:
				<input class="widefat"
						id="<?php echo $this -> get_field_id('facebook'); ?>"
						name="<?php echo $this -> get_field_name('facebook'); ?>"
						type="text"
						value="<?php echo $facebook; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('twitter'); ?>">Twitter link:
				<input class="widefat"
						id="<?php echo $this -> get_field_id('twitter'); ?>"
						name="<?php echo $this -> get_field_name('twitter'); ?>"
						type="text"
						value="<?php echo $twitter; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('googleplus'); ?>">Goggle+ link:
				<input class="widefat"
						id="<?php echo $this -> get_field_id('googleplus'); ?>"
						name="<?php echo $this -> get_field_name('googleplus'); ?>"
						type="text"
						value="<?php echo $googleplus; ?>" />
			</label>
		</p>
                <p>
                        <label for="<?php echo $this -> get_field_id('linkedin'); ?>">Linkedin link:
                                <input class="widefat"
                                                id="<?php echo $this -> get_field_id('linkedin'); ?>"
                                                name="<?php echo $this -> get_field_name('linkedin'); ?>"
                                                type="text"
                                                value="<?php echo $linkedin; ?>" />
                        </label>
                </p>
		<p>
			<label for="<?php echo $this -> get_field_id('youtube'); ?>">YouTube link:
				<input class="widefat"
						id="<?php echo $this -> get_field_id('youtube'); ?>"
						name="<?php echo $this -> get_field_name('youtube'); ?>"
						type="text"
						value="<?php echo $youtube; ?>" />
			</label>
		</p>
                <p>
                        <label for="<?php echo $this -> get_field_id('email'); ?>">Email link:
                                <input class="widefat"
                                                id="<?php echo $this -> get_field_id('email'); ?>"
                                                name="<?php echo $this -> get_field_name('email'); ?>"
                                                type="text"
                                                value="<?php echo $email; ?>" />
                        </label>
                </p>
		<p>
			<label for="<?php echo $this -> get_field_id('feed'); ?>">Feed link:
				<input class="widefat"
						id="<?php echo $this -> get_field_id('feed'); ?>"
						name="<?php echo $this -> get_field_name('feed'); ?>"
						type="text"
						value="<?php echo $feed; ?>" />
			</label>
		</p>
		<?php
		}
	
	
		// salva correttamente le impostazioni dell'utente
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['facebook'] = strip_tags( $new_instance['facebook'] );
			$instance['twitter'] = strip_tags( $new_instance['twitter'] );
			$instance['linkedin'] = strip_tags( $new_instance['linkedin'] );
			$instance['googleplus'] = strip_tags( $new_instance['googleplus'] );
			$instance['youtube'] = strip_tags( $new_instance['youtube'] );
			$instance['email'] = strip_tags( $new_instance['email'] );
			$instance['feed'] = strip_tags( $new_instance['feed'] );
			return $instance;
		}
	
	
		// manda a video il widget nel front-end
		function widget($args, $instance){
			extract($args, EXTR_SKIP);
			echo $before_widget;
			
			
			
			$facebook = apply_filters('widget_title', $instance['facebook']);
			$twitter = apply_filters('widget_title', $instance['twitter']);
			$linkedin = apply_filters('widget_title', $instance['linkedin']);
			$googleplus = apply_filters('widget_title', $instance['googleplus']);
			$youtube = apply_filters('widget_title', $instance['youtube']);
			$email = apply_filters('widget_title', $instance['email']);
			$feed = apply_filters('widget_title', $instance['feed']);
			
			
			if(!empty($facebook)){
				echo '<a href="' . $facebook . '" title="facebook" class="horizontal">
						<img src="' . plugins_url( 'img/Facebook48.png' , __FILE__ ) . '" alt="facebook" class="img" />
						</a>';
			}
			
			if(!empty($twitter)){
				echo '<a href="' . $twitter . '" title="twitter" class="horizontal">
						<img src="' . plugins_url( 'img/Twitter48.png' , __FILE__ ) . '" alt="twitter" class="img" />
						</a>';
			}
			
			if(!empty($linkedin)){
				echo '<a href="' . $linkedin . '" title="linkedin" class="horizontal">
						<img src="' . plugins_url( 'img/Linkedin48.png' , __FILE__ ) . '" alt="linkedin" class="img" />
						</a>';
			}
			
			if(!empty($googleplus)){
				echo '<a href="' . $googleplus . '" title="googleplus" class="horizontal">
						<img src="' . plugins_url( 'img/GooglePlus48.png' , __FILE__ ) . '" alt="googleplus" class="img" />
						</a>';
			}

                        if(!empty($email)){
                                echo '<a href="' . $email . '" title="youtube">
                                                <img src="' . plugins_url( '/img/YouTube48.png' , __FILE__ ) . '" alt="feed" class="img" />
                                                </a>';
                        }


                        if(!empty($email)){
                                echo '<a href="' . $email . '" title="email">
                                                <img src="' . plugins_url( '/img/Email48.png' , __FILE__ ) . '" alt="feed" class="img" />
                                                </a>';
                        }
			
			if(!empty($feed)){
				echo '<a href="' . $feed . '" title="feed">
						<img src="' . plugins_url( '/img/RSS48.png' , __FILE__ ) . '" alt="feed" class="img" />
						</a>';
			}
			
			echo $after_widget;
		}
}


function bhsw_frontend_enqueue_style() {
	wp_register_style( 'bhsw_style', plugins_url('/css/style.css', __FILE__) );
	wp_enqueue_style( 'bhsw_style');
}
add_action('wp_enqueue_scripts', 'bhsw_frontend_enqueue_style');

/*
function my_admin_notice(){
        global $pagenow;
	if ($agenow == 'plugins.php') {
	        echo '<div>
                        <p>Notifica solo per la Pagina plugins.</p>
                        </div>';
        }
}
add_action('admin_notices', 'my_admin_notice');
*/