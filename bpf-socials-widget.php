<?php
/**
 * Plugin Name: BPF Socials
 * Description: A widget that show socials icons.
 * Version: 0.1
 * Author: BERTOLOTTI Paolo Francesco
 * Author URI: 
 */

// aggiunge il widget all'elenco di quelli già presenti
add_action( 'widgets_init', 'bpf_socials_widget' );

function bpf_socials_widget() {
	register_widget( 'BPF_Socials_Widget' );
}

class BPF_Socials_Widget extends WP_Widget {
	
	/**
	 * costruttore
	 */
	function BPF_Socials_Widget() {
		$widget_ops = array('description' => 'A widget that show socials icons');
		$this->WP_Widget('bpf-socials-widget', 'BPF SOCIALS WIDGET', $widget_ops);
	}
	
	/*
	 * genera il form delle impostazioni del widget nel back-end
	 */
	function form( $instance ) {
		$direction = esc_attr( $instance['direction'] );
		$facebook = esc_attr( $instance['facebook'] );
		$twitter = esc_attr( $instance['twitter'] );
		$linkedin = esc_attr( $instance['linkedin'] );
		$googleplus = esc_attr( $instance['googleplus'] );
		$youtube = esc_attr( $instance['youtube'] );
		$pinterest = esc_attr( $instance['pinterest'] );
		$email =  esc_attr( $instance['email'] );
		$feed = esc_attr( $instance['feed'] );
		?>

		<p>
			<label for="<?php echo $this -> get_field_id('direction'); ?>">Dirtezione della barra:
				<select name="direction" id="direction" class="widefat">
					<option value="horizontal">Horizontal</option>
					<option value="vertical">Vertical</option>
				</select>
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('facebook'); ?>">Facebook link:
				<input class="widefat"
				id="<?php echo $this -> get_field_id('facebook'); ?>"
				name="<?php echo $this -> get_field_name('facebook'); ?>"
				type="text"
				value="<?php echo $facebook; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('twitter'); ?>">Twitter link:
				<input class="widefat"
				id="<?php echo $this -> get_field_id('twitter'); ?>"
				name="<?php echo $this -> get_field_name('twitter'); ?>"
				type="text"
				value="<?php echo $twitter; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('googleplus'); ?>">Goggle+ link:
				<input class="widefat"
				id="<?php echo $this -> get_field_id('googleplus'); ?>"
				name="<?php echo $this -> get_field_name('googleplus'); ?>"
				type="text"
				value="<?php echo $googleplus; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('linkedin'); ?>">Linkedin link:
				<input class="widefat"
				id="<?php echo $this -> get_field_id('linkedin'); ?>"
				name="<?php echo $this -> get_field_name('linkedin'); ?>"
				type="text"
				value="<?php echo $linkedin; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('youtube'); ?>">YouTube link:
				<input class="widefat"
				id="<?php echo $this -> get_field_id('youtube'); ?>"
				name="<?php echo $this -> get_field_name('youtube'); ?>"
				type="text"
				value="<?php echo $youtube; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('pinterest'); ?>">Pinterest link:
				<input class="widefat"
				id="<?php echo $this -> get_field_id('pinterest'); ?>"
				name="<?php echo $this -> get_field_name('pinterest'); ?>"
				type="text"
				value="<?php echo $pinterest; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('email'); ?>">Email link:
				<input class="widefat"
				id="<?php echo $this -> get_field_id('email'); ?>"
				name="<?php echo $this -> get_field_name('email'); ?>"
				type="text"
				value="<?php echo $email; ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo $this -> get_field_id('feed'); ?>">Feed link:
				<input class="widefat"
				id="<?php echo $this -> get_field_id('feed'); ?>"
				name="<?php echo $this -> get_field_name('feed'); ?>"
				type="text"
				value="<?php echo $feed; ?>" />
			</label>
		</p>
		<?php
	}
	
	
	/**
	 * salva correttamente le impostazioni dell'utente
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['direction'] = strip_tags( $new_instance['direction'] );
		$instance['facebook'] = strip_tags( $new_instance['facebook'] );
		$instance['twitter'] = strip_tags( $new_instance['twitter'] );
		$instance['linkedin'] = strip_tags( $new_instance['linkedin'] );
		$instance['googleplus'] = strip_tags( $new_instance['googleplus'] );
		$instance['youtube'] = strip_tags( $new_instance['youtube'] );
		$instance['pinterest'] = strip_tags( $new_instance['pinterest'] );
		$instance['email'] = strip_tags( $new_instance['email'] );
		$instance['feed'] = strip_tags( $new_instance['feed'] );
		return $instance;
	}
	
	
	/**
	 * manda a video il widget nel front-end
	 */
	function widget($args, $instance){
		extract($args, EXTR_SKIP);
		echo $before_widget;

		$direction = apply_filters('widget_title', $instance['direction']);
		$facebook = apply_filters('widget_title', $instance['facebook']);
		$twitter = apply_filters('widget_title', $instance['twitter']);
		$linkedin = apply_filters('widget_title', $instance['linkedin']);
		$googleplus = apply_filters('widget_title', $instance['googleplus']);
		$youtube = apply_filters('widget_title', $instance['youtube']);
		$pinterest = apply_filters('widget_title', $instance['pinterest']);
		$email = apply_filters('widget_title', $instance['email']);
		$feed = apply_filters('widget_title', $instance['feed']);

		echo '<div';
		if ($direction == 'vertical') {
			echo 'style="float:left;"';
		}
		echo '>';

		if(!empty($facebook)){
			echo '<a href="' . $facebook . '" title="facebook" class="'.$direction.'"><img src="' . plugins_url( 'img/Facebook48.png' , __FILE__ ) . '" alt="facebook" class="img" /></a>';
		}

		if(!empty($twitter)){
			echo '<a href="' . $twitter . '" title="twitter" class="'.$direction.'"><img src="' . plugins_url( 'img/Twitter48.png' , __FILE__ ) . '" alt="twitter" class="img" /></a>';
		}

		if(!empty($linkedin)){
			echo '<a href="' . $linkedin . '" title="linkedin" class="'.$direction.'"><img src="' . plugins_url( 'img/Linkedin48.png' , __FILE__ ) . '" alt="linkedin" class="img" /></a>';
		}

		if(!empty($googleplus)){
			echo '<a href="' . $googleplus . '" title="googleplus" class="'.$direction.'"><img src="' . plugins_url( 'img/GooglePlus48.png' , __FILE__ ) . '" alt="googleplus" class="img" /></a>';
		}

		if(!empty($youtube)){
			echo '<a href="' . $email . '" title="youtube" class="'.$direction.'"><img src="' . plugins_url( '/img/YouTube48.png' , __FILE__ ) . '" alt="youtube" class="img" /></a>';
		}

		if(!empty($pinterest)){
			echo '<a href="' . $email . '" title="pinterest" class="'.$direction.'"><img src="' . plugins_url( '/img/Pinterest48.png' , __FILE__ ) . '" alt="pinterest" class="img" /></a>';
		}

		if(!empty($email)){
			echo '<a href="' . $email . '" title="email" class="'.$direction.'"><img src="' . plugins_url( '/img/Email48.png' , __FILE__ ) . '" alt="email" class="img" /></a>';
		}

		if(!empty($feed)){
			echo '<a href="' . $feed . '" title="feed" class="'.$direction.'"><img src="' . plugins_url( '/img/RSS48.png' , __FILE__ ) . '" alt="feed" class="img" /></a>';
		}

		echo $after_widget;

		echo "</div>";
	}
}

/**
 * aggiunge un foglio di stile al plugin
 */
function bhsw_frontend_enqueue_style() {
	wp_register_style( 'bhsw_style', plugins_url('/css/style.css', __FILE__) );
	wp_enqueue_style( 'bhsw_style');
}
add_action('wp_enqueue_scripts', 'bhsw_frontend_enqueue_style');

/*
function my_admin_notice(){
        global $pagenow;
	if ($agenow == 'plugins.php') {
	        echo '<div>
                        <p>Notifica solo per la Pagina plugins.</p>
                        </div>';
        }
}
add_action('admin_notices', 'my_admin_notice');
*/